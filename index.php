<!DOCTYPE html>

<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <link rel="preconnect" href="https://fonts.gstatic.com" />
  <link href="https://fonts.googleapis.com/css2?family=Didact+Gothic&family=Francois+One&family=Poppins&display=swap"
    rel="stylesheet" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css" />
  <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" />
  <link href="node_modules/Hover-master/css/hover.css rel="stylesheet" media="all" />

  <link rel="stylesheet" href="css\style.css" />
  <link rel="stylesheet" href="style.css" />
  <title>Ludmila Ciupac</title>
</head>

<body>
  <div data-aos="fade-down" class="shadow p-3 bg-light navigation">
    <div class="container-fluid">
      <div class="row">
        <nav id="menu" class="col navbar navbar-expand-lg navbar-light shift">
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div id="navbarContent" class="collapse navbar-collapse justify-content-center">
            <ul class="navbar-nav">
              <li class="nav-item">
                <a class="nav-link" href="#accueil">ACCUEIL</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#presentation">PRÉSENTATION</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#competences">COMPÉTENCES</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#formations">FORMATIONS</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#experiences">EXPÉRIENCES</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#contact">CONTACT</a>
              </li>
            </ul>
          </div>
          <div>
            <a class="navbar-brand col" href="index.php" id="logoNav">
              LUDMILA CIUPAC
            </a>
          </div>
        </nav>
      </div>
    </div>
  </div>

  <header id="accueil" class="container-fluid accueil">
    <div class="container-fluid my-5 d-flex justify-content-center justify-content-lg-start">
      <img class="shadow p-3 mb-5 text-center rounded-circle" id="ludmila" src="img/ludmila.jpg"
      alt="Ludmila Ciupac" data-aos="flip-up" />
  </div>
      <div class="row d-flex justify-content-center">

        <div id="identite" class="col-12 shadow p-3 mb-5 my-auto text-center rounded">
          <h1>Ludmila Ciupac</h1>
          <h2>Développeuse Web junior</h2>
        </div>
    </div>
  </header>

  <section class="container-fluid mt-5 mb-5">
    <div class="container">
      <div class="row">
        <div class="col-12 col-lg-3 text-center d-flex justify-content-center justify-content-lg-between">
        <a href="CV/CV-ludmila.pdf" download ><button type="button" class="btn btn-success hvr-float-shadow mb-3">
            <i class="fas fa-arrow-alt-circle-down"></i> <span>CV</span>
          </button></a>
        </div>
        <div class="col-12 col-lg-3 text-center d-flex justify-content-center justify-content-lg-between">
        <a href="https://www.linkedin.com/in/ludmila-ciupac-143250aa/"><button type="button" class="btn btn-success hvr-float-shadow mb-3">
            <i class="fab fa-linkedin"></i> <span>LINKEDIN</span>
          </button></a>
        </div>
        <div class="col-12 col-lg-3 text-center d-flex justify-content-center justify-content-lg-between">
        <a href="https://gitlab.com/LudmilaCiupac"><button type="button" class="btn btn-success hvr-float-shadow mb-3">
        <i class="fab fa-gitlab"></i> <span>GITLAB</span>
          </button></a>
        </div>
        <div class="col-12 col-lg-3 text-center d-flex justify-content-center justify-content-lg-between">
        <a href="https://portefolio.ludmila-ciupac.labo-ve.fr/"><button type="button" class="btn btn-success hvr-float-shadow mb-3">
           <i class="fas fa-globe"></i> <span>PORTEFOLIO</span>
          </button></a>
        </div>
      </div>
    </div>
    <hr>
  </section>

  <hero class="container-fluid">
    <div class="container">
      <div class="row my-3 text-center">
        <div class="col">
          <h3 id="presentation">PRÉSENTATION</h3>
        </div>
      </div>
    </div>

    <div class="container">
      <div class="row my-3 text-left">
        <div class="col">
          <p class="text-center">
          Curieuse, motivée et volontaire, j'ai decidé de réorienter ma carrière vers l'informatique. <br>
            J'ai la chance de me réaliser dans ce parcours de formation qui me permet d'acquérir 
            des compétences et expériences.<br>
             Mon objectif d'être web-développeuse est soutenu par des qualités 
            pédagogiques eprouvées. <br>
            En fin de formation, en avril 2021, je recherche un stage qui me permettra de valider mes acquis 
            et vous apporter mon nouveau regard.
          </p>
        </div>
      </div>
      <div class="row my-3 text-left">
        <div data-aos="flip-left"
          class="col-12 col-lg-3 mx-auto bg-white rounded shadow p-3 mb-5 border border-success">
          <h4 class="text-center">Langues</h4>
          <div class="lang" data-aos="fade-right">
        <div class="align">
            <div class="chevron-right">
              <i class="fas fa-arrow-alt-circle-right logo2"></i>
        </div>
        <p class="langues">Français</p>
      </div>
      
        <div class="align">
            <div class="chevron-right">
              <i class="fas fa-arrow-alt-circle-right logo2"></i>
        </div>
        <p class="langues">Anglais</p>
      </div>
    
        <div class="align">
            <div class="chevron-right">
              <i class="fas fa-arrow-alt-circle-right logo2"></i>
        </div>
        <p class="langues">Roumain</p>
      </div>
      
        <div class="align">
            <div class="chevron-right">
              <i class="fas fa-arrow-alt-circle-right logo2"></i>
        </div>
        <p class="langues">Russe</p>
      </div>
          </div>
      

        </div>
        <div data-aos="flip-right"
          class="col-12 col-lg-3 mx-auto bg-white rounded shadow p-3 mb-5 border border-success">
          <h4 class="text-center">Centres d'intérêt</h4>
          <ul class="liste2" data-aos="fade-left">
            <li><i class="fas fa-biking logo"></i> Sports</li>
            <li><i class="fas fa-book logo"></i> Lecture</li>
            <li><i class="fas fa-plane logo"></i> Voyage</li>
          </ul>
        </div>
      </div>
    </div>
    <hr />
  </hero>

  <section class="container-fluid">
    <div class="container">
      <div class="row my-3 text-center">
        <div class="col">
          <h3 id="competences">COMPÉTENCES</h3>
        </div>
      </div>
    </div>
    <div data-aos="zoom-in-up" class="contComp">
            <i class="fab fa-html5 logos red"></i>
            <i class="fab fa-css3-alt logos blue"></i>
            <i class="fab fa-js-square logos green"></i>
            <i class="fab fa-php logos yellow"></i>
            <i class="fas fa-database logos purple"></i>
    </div>
  
      <div class="row my-3 text-left">
        <div class="col-12 col-lg-3 mx-auto bg-white rounded shadow p-3 mb-5 border border-success" data-aos="flip-left">
          <h4 class="text-center">Soft Skills</h4>
          <ul id="fade" class="liste2">
            <li>
             Autonomie
            </li>
            <li>
              Esprit d'équipe
            </li>
            <li>
             Adaptabilité
            </li>
            <li>
            Curiosité
          </li>
          </ul>
        </div>
        <div class="col-12 col-lg-3 mx-auto bg-white rounded shadow p-3 mb-5 border border-success" data-aos="flip-right">
          <h4 class="text-center">Hard Skills</h4>
          <ul id="fade" class="liste2">
            <li>Maquetter une application</li>
            <li>Réaliser une interface utilisateur</li>
            <li>Créer une base de données</li>
          </ul>
        </div>
      </div>
    </div>
    <hr />
  </section>

  <section class="container-fluid">
    <div class="container">
      <div class="row my-3 text-center">
        <div class="col">
          <h3 id="formations">FORMATIONS</h3>
        </div>
      </div>
    </div>

    <div class="container d-flex justify-content-center">
      <div class="row my-3 text-center mx-auto d-flex justify-content-center">
        <div class="row my-3 text-center shadow mb-5 bg-success w-100 rounded">
          <div data-aos="fade-right" class="col-12 col-lg-4 bg-danger p-3 rounded-left my-auto">
            <p><strong>2021<br />Titre professionnel Développeur Web</strong>
          </div>
          </p>
          <div data-aos="fade-left" class="col-12 col-lg-8 p-3 rounded-right my-auto">
            <p>École Numérique Ardéchoise, Le Cheylard</p>
          </div>
        </div>
        <div class="row my-3 text-center shadow mb-5 bg-success w-100 rounded">
          <div data-aos="fade-right" class="col-12 col-lg-4 bg-danger p-3 rounded-left my-auto">
            <p><strong>2008<br />Diplôme de Licence en Français Langue
                Étrangère</strong></p>
          </div>
          <div data-aos="fade-left" class="col-12 col-lg-8 p-3 rounded-right my-auto">
            <p>Université Stendhal III, Grenoble</p>
          </div>
        </div>
        <div class="row my-3 text-center shadow mb-5 bg-success w-100 rounded">
          <div data-aos="fade-right" class="col-12 col-lg-4 bg-danger p-3 rounded-left my-auto">
            <p><strong>2005<br />Diplôme de Français des Affaires</strong></p>
          </div>
          <div data-aos="fade-left" class="col-12 col-lg-8 p-3 rounded-right my-auto">
            <p>Chambre de Commerce et d'Industrie de Paris</p>
          </div>
        </div>
        <div class="row my-3 text-center shadow mb-5 bg-success w-100 rounded">
          <div data-aos="fade-right" class="col-12 col-lg-4 bg-danger p-3 rounded-left my-auto">
            <p><strong>2004<br />Diplôme de Maîtrise</strong></p>
          </div>
          <div data-aos="fade-left" class="col-12 col-lg-8 p-3 rounded-right my-auto">
            <p>Langue et Littérature Française/ Langue Anglaise<br>
              Université d'Etat de Moldavie</p>
          </div>
        </div>
        <div class="row my-3 text-center shadow mb-5 bg-success w-100 rounded">
          <div data-aos="fade-right" class="col-12 col-lg-4 bg-danger p-3 rounded-left my-auto">
            <p><strong>2000<br />Diplôme de Baccalauréat</strong></p>
          </div>
          <div data-aos="fade-left" class="col-12 col-lg-8 p-3 rounded-right my-auto">
            <p>Lycée « Vlastar » ville de Chisinau /Moldavie</p>
          </div>
        </div>
      </div>
    </div>
    <hr />
  </section>

  <section class="container-fluid">
    <div class="container">
      <div class="row my-3 text-center">
        <div class="col">
          <h3 id="experiences">EXPÉRIENCES</h3>
        </div>
      </div>
    </div>

    <div class="container d-flex justify-content-center">
      <div class="row my-3 text-center mx-auto d-flex justify-content-center">
        <div class="row my-3 text-center shadow mb-5 bg-danger w-100 rounded">
          <div data-aos="fade-right" class="col-12 col-lg-4 bg-success p-3 rounded-left my-auto">
            <p><strong>2020<br />Expert judiciaire</strong></p>
          </div>
          <div data-aos="fade-left" class="col-12 col-lg-8 p-3 rounded-right my-auto">
            <p>Traduction et interprétariat en langues Moldave/Roumain/Russe auprès de la <strong>Cour d’Appel de
                Nîmes</strong></p>
          </div>
        </div>
        <div class="row my-3 text-center shadow mb-5 bg-danger w-100 rounded">
          <div data-aos="fade-right" class="col-12 col-lg-4 bg-success p-3 rounded-left my-auto">
            <p><strong>2016 - 2020<br />CEFORA SCOOP</strong></p>
          </div>
          <div data-aos="fade-left" class="col-12 col-lg-8 p-3 rounded-right my-auto">
            <p><strong>Formatrice en Français Langue Etrangère</strong> auprès d'un public de demandeurs d'emploi.
              Privas/Tournon sur Rhône/Colombes/Gennevilliers</p>
          </div>
        </div>
        <div class="row my-3 text-center shadow mb-5 bg-danger w-100 rounded">
          <div data-aos="fade-right" class="col-12 col-lg-4 bg-success p-3 rounded-left my-auto">
            <p><strong>2012 - 2016<br />SDA Aéroport de Paris</strong></p>
          </div>
          <div data-aos="fade-left" class="col-12 col-lg-8 p-3 rounded-right my-auto">
            <p><strong>Conseillère de vente articles de luxe</strong> - SDA Duty Free Aéroport Charles de Gaulle</p>
          </div>
        </div>
        <div class="row my-3 text-center shadow mb-5 bg-danger w-100 rounded">
          <div data-aos="fade-right" class="col-12 col-lg-4 bg-success p-3 rounded-left my-auto">
            <p><strong>2006 - 2020<br />Commissariats de Police</strong></p>
          </div>
          <div data-aos="fade-left" class="col-12 col-lg-8 p-3 rounded-right my-auto">
            <p><strong>Missions ponctuelles en tant qu'interprète</strong> et traductrice - Commissariats de police des
              Haut de Seine et TGI de Nanterre et Privas</p>
          </div>
        </div>
      </div>
    </div>
    <hr />
  </section>

  <footer class="container-fluid">
    <div class="container">
      <div class="row my-3 text-center">
        <div class="col">
          <h3 id="contact">CONTACT</h3>
        </div>
      </div>
    </div>
    <div class="container">
    <div class="row">
        <div class="col-12 col-lg-3 text-center d-flex justify-content-center justify-content-lg-between" data-aos="fade-right">
        <a href="CV/CV-ludmila.pdf" download ><button type="button" class="btn btn-success hvr-float-shadow mb-3">
            <i class="fas fa-arrow-alt-circle-down"></i> <span>CV</span>
          </button></a>
        </div>
        <div class="col-12 col-lg-3 text-center d-flex justify-content-center justify-content-lg-between" data-aos="fade-right">
        <a href="https://www.linkedin.com/in/ludmila-ciupac-143250aa/"><button type="button" class="btn btn-success hvr-float-shadow mb-3">
            <i class="fab fa-linkedin"></i> <span>LINKEDIN</span>
          </button></a>
        </div>
        <div class="col-12 col-lg-3 text-center d-flex justify-content-center justify-content-lg-between" data-aos="fade-left">
        <a href="https://gitlab.com/LudmilaCiupac"><button type="button" class="btn btn-success hvr-float-shadow mb-3">
           <i class="fab fa-gitlab"></i> <span>GITLAB</span>
          </button></a>
        </div>
        <div class="col-12 col-lg-3 text-center d-flex justify-content-center justify-content-lg-between" data-aos="fade-left">
        <a href="https://portefolio.ludmila-ciupac.labo-ve.fr/"><button type="button" class="btn btn-success hvr-float-shadow mb-3">
            <i class="fas fa-globe"></i> <span>PORTEFOLIO</span>
          </button></a>
        </div>
      </div>
    </div>
      
      <div class="container-fluid map">
      <div class="formcont" data-aos="zoom-in-up">

    <form action="index.php" method="post">

        <?php
        if (empty($_POST)) : ?>
          <label>Prénom</label>
          <br />
          <input class="input" type="text" name="prenom" pattern="[A-zÀ-ÖØ-öø-ÿ- ]{2,}" maxlength="50" required />
          <br />
          <label>Nom</label>
          <br />
          <input class="input" type="text" name="nom" pattern="[A-zÀ-ÖØ-öø-ÿ- ]{2,}" maxlength="50" required />
          <br />
          <label>E-mail</label>
          <br />
          <input class="input" type="email" name="mail" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" minlength="7" required />
          <br />
          <label>Objet du message</label>
          <br />
          <input class="input" type="text" name="sujet" pattern="[A-zÀ-ÖØ-öø-ÿ0-9- ]{2,}" maxlength="50" required />
          <br />
          <label>Message</label>
          <br />
          <textarea class="input" name="message" rows="5" cols="33" pattern="[A-zÀ-ÖØ-öø-ÿ- ]{2,}" maxlength="1000" required></textarea>
          </br> </br>
          <input type="submit" value="Envoyer" id="button" style="color:black;" />
          </br> </br>
    </form>
  
    <?php else :
          //Comment valider les données en provenance de mon formulaire ?
  
          //1 Les données obligatoires sont elles présentes ?
          if (isset($_POST["mail"]) && isset($_POST["message"]) && isset($_POST["prenom"]) && isset($_POST["nom"]) && isset($_POST["sujet"])) {
  
            //2 Les données obligatoires sont-elles remplies ?
            if (!empty($_POST["mail"]) && !empty($_POST["message"]) && !empty($_POST["prenom"]) && !empty($_POST["nom"]) && !empty($_POST["sujet"])) {
  
              //3 Les données obligatoires ont-elles une valeur conforme à la forme attendue ?
              if (filter_var($_POST["mail"], FILTER_SANITIZE_EMAIL)) {
  
                $_POST["mail"] = filter_var($_POST["mail"], FILTER_SANITIZE_EMAIL);
  
                if (filter_var($_POST["message"], FILTER_SANITIZE_FULL_SPECIAL_CHARS)) {
                  $_POST["message"] = filter_var($_POST["message"], FILTER_SANITIZE_FULL_SPECIAL_CHARS);
  
                  if (filter_var($_POST["prenom"], FILTER_SANITIZE_STRING) && filter_var($_POST["nom"], FILTER_SANITIZE_STRING) && filter_var($_POST["sujet"], FILTER_SANITIZE_STRING)) {
                    $_POST["prenom"] = filter_var($_POST["prenom"], FILTER_SANITIZE_STRING);
                    $_POST["nom"] = filter_var($_POST["nom"], FILTER_SANITIZE_STRING);
                    $_POST["sujet"] = filter_var($_POST["sujet"], FILTER_SANITIZE_STRING);
  
                    //5 afficher le mail et le message
    ?>
              <p>Bonjour <?php printf('%s', $_POST["prenom"]); ?>, votre message a été envoyé et vous serez contacté dans les plus bref délais !</p>
              <meta http-equiv="refresh" content="1;URL=#contact">
    <?php
                    //6 utiliser la fonction mail() pour envoyer le message vers botre boîte mail
                    $to = "lciupac07@gmail.com";
                    $subject = $_POST["sujet"];
                    $message = 'Vous avez un nouveau message de ' . $_POST["prenom"] . ' ' . $_POST["nom"];
                    $message .= PHP_EOL . 'Contenu du message : ' . $_POST["message"];
                    if ($_POST["mail"]) {
                      $message .= PHP_EOL . 'Email : ' . $_POST["mail"];
                    }
                    $headers = 'From: ' . 'ludmila-ciupac@labo-ve.fr' . PHP_EOL .
                      'Reply-To: ' . $_POST["mail"] . PHP_EOL .
                      'Content-Type: text/html; charset=UTF-8' . PHP_EOL .
                      'X-Mailer: PHP/' . phpversion();
                    mail($to, $subject, $message, $headers);
                  } else {
                    print("Fournissez des informations valides svp !");
                  }
                } else {
                  print("La forme de votre message a outrepassé toutes nos prédictions !");
                }
              } else {
                print("Saisir une adresse email valide !");
              }
            } else {
              print("Il faut saisir les valeurs obligatoires !");
            }
          } else {
            print("RENDS-MOI MES CODES PETIT DÉGLINGO !");
          }
    ?>
  <?php
        endif;
  ?>
  </div>
    </div>
  </footer>

  <!-- Optional JavaScript -->

  <!-- jQuery first, then Popper.js, then Bootstrap JS -->

  <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
    integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
    crossorigin="anonymous"></script>

  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
    integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
    crossorigin="anonymous"></script>

  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
    integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
    crossorigin="anonymous"></script>

  <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
  <script>
    AOS.init();
  </script>
</body>

</html>